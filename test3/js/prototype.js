let p = {age:18};

let o4 = {a:1};
// o4.prototype = p;
// console.log(o4);
// console.log(o4.age);
// console.log(o4.prototype);
// console.log(p.age);

o4.__proto__ = p;
console.log('---------');
console.log(o4);
console.log(o4.age);


function Supper(){
	this.age = 80;
}

function Sub(){
	this.height = '1m';
}
Sub.prototype = p;

let instance = new Sub();
console.log('---------===');
console.log(instance.age);
console.log(instance.prototype);
console.log(instance.__proto__);
console.log(instance.__proto__.__proto__);

function Sub2(){
	this.height = '1m';
}
Sub2.prototype = new Supper();
let instance2 = new Sub2();
console.log('---------2===');
console.log(instance2.age);
console.log(instance2.__proto__);
console.log(typeof instance2.__proto__);
console.log(instance2.__proto__.__proto__);