// function a(){
// 	let va = 'va';

// 	console.log('a:'this);

// 	function b(){
// 		console.log('b之中：' + this);
// 	}

// 	return b;
// }

class ThisClass {
  constructor() {
    this.a = 0;
    this.b = 2;
  }

  println() {
    console.log(this.a);
  }
}
let tc = new ThisClass();
tc.println();

let test = {a: 'test'};
tc.println.call(test);
console.log('=====');
test.pl = tc.println;
test.pl();

/**
 * 谁调用谁就是this
 * call和apply可以后面的对象调用前面的方法
 * 
 */