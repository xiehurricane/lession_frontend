let o3 = {
  a: 1,
  b: 2,
  c: "哈哈",
  _ww: 3, 
  f() {
    console.log("我是简写的方法");
  },
  ff: function(){
	console.log("我和简写的方法一样效果");
  },
  f3: () => {
	console.log("我和简写的方法一样效果===");
  },
  
  get ww(){
	return this._ww;
  },
  set ww(nvalue) {
	console.log('我是简写的setter方法');
	
	this._ww = nvalue;
  }
};

Object.defineProperty(o3, "d", {
  set: function(newvalue) {
    console.log(`===111== ${newvalue}`);

    this._d = newvalue;
    this.a = newvalue;
    // return newvalue;
  },
  get: function() {
    return this._d;
  }
});

console.log(o3);
o3.d = "我在哪里";

console.log(o3);
console.log(o3._d);
console.log(o3.d);
o3.f();
o3.ff();
o3.f3();
o3.ww = 333