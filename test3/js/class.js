
class FirstClass {
	constructor(){
		this.a = 1;
		this.b = 2;
	}

	calc(f){
		console.log('=='+ (this.a + f));
	}

}

// console.log(typeof FirstClass);
// console.log(FirstClass.prototype);
// for (var propName in FirstClass.prototype) { console.log(propName); }

let insF = new FirstClass();
// console.log(insF.calc(9));

function TwoFunction(){
	this.a = 1;
	this.b = 2;
}
TwoFunction.prototype.calc = function(f){
	console.log('=='+ (this.a + f));
}

class SubClass extends FirstClass{

	myprint(){
		console.log('-----' + this.a);
	}
}

let ins = new SubClass();
ins.myprint();
ins.calc(8);