function SuperType() {
  this.property = true;
}
SuperType.prototype.getSuperValue = function() {
  return this.property;
};
function SubType() {
  this.subproperty = false;
}
//继承了 SuperType
SubType.prototype = new SuperType();
console.log('-SubType.prototype---'+ JSON.stringify(SubType.prototype));
SubType.prototype.getSubValue = function() {
  return this.subproperty;
};
var instance = new SubType();
console.log(instance.getSuperValue()); //true
console.log('---------');
console.log(instance.__proto__);
console.log(instance.__proto__.__proto__);
console.log(instance.__proto__.__proto__.__proto__);