function f(a,b = 'abc',c){
	console.log(a);
	console.log(b);
	console.log(c);
	console.log(arguments);
	console.log(arguments[0]);
}

f(1)

function f2(...c){
	console.log('--------');
	console.log(c);
	console.log(arguments);
	console.log(arguments[0]);
}

f2(1,2,3)