let o1 = { age: 10 };
console.log(o1.age);

// ----------------------
function o2() {
  this.age = 12;
}
console.log(o2.age);
let tmp = new o2();
console.log(tmp.age);

// ----------------------

let o3 = new Object();
o3.age = 18;

console.log(o3.age);

// ----------------------
function O4() {}
O4.prototype.age = 20;
console.log(O4.age);
tmp = new O4();
console.log(tmp.age);


