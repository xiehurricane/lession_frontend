
// 总结：
// 调用链自己环境的隐藏


var object = {
  name: "Object",
  getNameFunc: function() {
    return function() {
		this.name = 'f';
      return this.name;
    };
  }
};

console.log(object.getNameFunc()());

function a(){
	let va = 'va';


	function b(){
		console.log('b之中：' + va);
	}

	return b;
}


console.log(typeof a());
let vc = a();

vc();

// b *
// a
// window *

console.log("this "+ this);
console.log("this "+ typeof this);

function a(){
	let va = 'va';

	console.log("this ");
	function b(){
		this.testb = "bbbbb";
		

		return this;
	}
	

	return b;
}







